ID: 1512016
Name: Lê Minh Ân

+ Các chức năng đã thực hiện:
1. Cho phép thay đổi kích thước của treeview và listview, kéo theo nhau.
2. Status bar xuất hiện thông tin của ổ đĩa
3. Lưu lại kích thước cửa sổ màn hình chính và nạp lại khi chương trình chạy lên.

+Luồng sự kiện chính : chạy chương trình lên sẽ hiện lên node My Computer và các node con sổ xuống là các ổ đĩa ở treeview bên trái, khi nhấn một ổ đĩa dạng thu gọn ở treeview bên trái sẽ xổ xuống các thư mục con.

+Link bitbucket:
SSH: git clone git@bitbucket.org:minhan97/windev.git
HTPPS: git clone https://minhan97@bitbucket.org/minhan97/windev.git