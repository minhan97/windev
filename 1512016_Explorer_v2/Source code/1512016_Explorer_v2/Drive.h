#pragma once
#include <windows.h>
#include <tchar.h>
#include "DriveSize.h"

#define MAX_BUFFER_LEN 105

//Define for GetVolumeInformation
#define VOLUME_NAME_SIZE		MAX_BUFFER_LEN
#define VOLUME_SERIAL_NUMBER	NULL
#define MAX_COMPONENT_LEN		NULL
#define FILE_SYSTEM_FLAG		NULL
#define FILE_SYSTEM_NAME_BUFF	NULL //Name of File System (NTFS, FAT,...)
#define FILE_SYSTEM_NAME_SIZE	0  //Size of buffer for file system name

//For allocate memory
#define MAX_TYPE_LEN		20
#define MAX_DRIVE_LETTER	4
#define MAX_VOLUME_NAME		30
#define MAX_DISPLAY_NAME	30


//For type of drive
#define FIXED_DRIVE		_T("Local Disk")
#define REMOVABLE_DRIVE _T("Removable Drive")
#define CD_ROM			_T("CD-ROM")
#define REMOTE_DRIVE	_T("Network Drive")

#define IDI_FOLDER 0
#define IDI_UNKNOWN_FILE 1
#define IDI_DESKTOP 2
#define IDI_MYCOMPUTER 3

#define IDI_FLOPPY 4
#define IDI_USB 5
#define IDI_HDD 6
#define IDI_CD  7 

class Drive
{
	TCHAR** mDriveLetter;
	TCHAR** mVolumeLabel;
	TCHAR** mDriveType;
	int mNumberOfDrive;

	DriveSize** mDriveSize;
	DriveSize* getDriveSize(int i);

	int* m_nIconIndex;

public:
	Drive();
	~Drive();

	TCHAR* getDriveLetter(const int &);
	TCHAR* getDisplayName(const int &);
	int getCount();

	int GetIconIndex(const int &i);
	void getSystemDrives();

	TCHAR* getDriveType(int);
	LPWSTR getTotalSize(int);
	LPWSTR getFreeSpace(int);
};