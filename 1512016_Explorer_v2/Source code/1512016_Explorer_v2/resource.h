//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by 1512016_Explorer_v2.rc
//
#define IDC_MYICON                      2
#define IDD_MY1512016_EXPLORER_V2_DIALOG 102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDC_TREEVIEW                    104
#define IDM_EXIT                        105
#define IDC_LISTVIEW                    105
#define IDC_STATUSBAR                   106
#define IDI_MY1512016_EXPLORER_V2       107
#define IDI_SMALL                       108
#define IDC_MY1512016_EXPLORER_V2       109
#define IDR_MAINFRAME                   128
#define IDI_ICON0                       138
#define IDI_ICON1                       139
#define IDI_ICON2                       140
#define IDI_ICON3                       141
#define IDI_ICON4                       146
#define IDI_ICON5                       147
#define IDI_ICON6                       148
#define IDI_ICON7                       149
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        150
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
