// 1512016_Explorer_v2.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "1512016_Explorer_v2.h"
#include <CommCtrl.h>

#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment( lib, "ComCtl32.lib" )

//For StrCpy, StrNCat
#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")
#include <shellapi.h>

#define IDI_FOLDER 0
#define IDI_UNKNOWN_FILE 1
#define IDI_DESKTOP 2
#define IDI_MYCOMPUTER 3
#define IDI_FLOPPY 4
#define IDI_USB 5
#define IDI_HDD 6
#define IDI_CD  7 

#define MAX_LOADSTRING 100
#define DEFAULT_ICON_INDEX 0
#define MAX_PATH_LEN 10240

//For init lv column
#define LVCOL_DRIVE_TYPE	0
#define LVCOL_FOLDER_TYPE	1

#define MAIN_WINDOW_CLASS		"Main Window"
#define LEFT_WINDOW_CLASS		"Left Window"
#define RIGHT_WINDOW_CLASS		"Right Window"
#define BUTTON_CLASS			"Button"	
#define RESET					256
#define WIDTH_ADJUST			2
#define MIN_RESIZABLE_WIDTH		400
#define MIN_RESIZABLE_HEIGHT	350

#define	WINDOW_WIDTH			800
#define	WINDOW_HEIGHT			600

#define LEFT_WINDOW_WIDTH		200
#define	SPLITTER_BAR_WIDTH		2

#define	LEFT_MINIMUM_SPACE		150
#define	RIGHT_MINIMUM_SPACE		500

#define	TOP_POS					30
#define	BOTTOM_POS				30

//Height of the static text is equal to TOP_POS
#define	STATIC_TEXT_HEIGHT		TOP_POS


//Macros for width and height of the buttons
#define	CLOSE_BUTTON_WIDTH		80

#define	COLOR_BUTTON_WIDTH		250

#define	BUTTON_HEIGHT			26


//Macros for alighning buttons
#define	SPACE_BUTTON_RIGHT		100
#define	SPACE_BUTTON_BOTTOM		50
#define	BUTTON_ADJUST			10

#define SET						1
#define CLEAR					0

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name
HINSTANCE g_hInstance;
HWND	g_hWnd;
HWND	g_hTreeView;
HWND	g_hListView;
HWND	g_hStatusBar;
RECT rect;
Drive* g_Drive;
const int BUFFERSIZE = 260;
WCHAR curPath[BUFFERSIZE];
WCHAR configPath[BUFFERSIZE];

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

HWND createListView(long lExtStyle, HWND parentWnd, long ID, HINSTANCE hParentInst, int x, int y, int nWidth, int nHeight);
void loadToListView(Drive* drive, HWND m_hListView);
void loadListviewItemAt(LPCWSTR path, HWND m_hParent, HWND m_hListView, Drive *drive); //Load listview item at dir path provided
void loadOrExecSelected(HWND m_hListView); //Load selected directory or execute selected file
void loadDirItemToListview(HWND m_hParent, HWND m_hListView, LPCWSTR path); //Load directory item to Listview
void initListviewColumn(HWND m_hListView, int type); //Initialize Listview column (size, header text,...)
LPCWSTR getPath(HWND m_hListView, int iItem);
void imageListView();

HWND createTreeView(long lExtStyle, HWND parentWnd, long ID, HINSTANCE hParentInst, int x, int y, int nWidth, int nHeight);
void loadToTreeView(Drive* drive, HWND m_hTreeView);
void loadExpandedChild(HTREEITEM hCurrSelected, HWND m_hTreeView); //Load all child and child of child items in treeview
LPCWSTR getPath(HTREEITEM hItem, HWND m_hTreeView); //Get dir path of an item in Treeview
void loadTreeviewItemAt(HTREEITEM &hParent, LPCWSTR path, HWND m_hTreeView); //Load treeview item at dir path provided
void imageTree();

LPWSTR convertTimeStampToString(const FILETIME &ftLastWrite); //Convert Active Directory timestamps (LDAP/Win32 FILETIME) to DateTime in String

void saveIniconfig(int nwidth, int nheight);
void loadIniconfig(int &nwidth, int &nheight);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_MY1512016_EXPLORER_V2, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MY1512016_EXPLORER_V2));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON3));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_MY1512016_EXPLORER_V2);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_ICON2));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//

int firstWidth = 1000, firstHeight = 600;
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   GetCurrentDirectory(BUFFERSIZE, curPath);
   wsprintf(configPath, L"%s\\config.ini", curPath);

   hInst = hInstance; // Store instance handle in our global variable
   loadIniconfig(firstWidth, firstHeight);
   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
	   CW_USEDEFAULT, 0, firstWidth, firstHeight, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	TCHAR *text = new TCHAR[256];
	static  int   nleftWnd_width = 0;
	static  BOOL  xSizing;
	static HCURSOR	hcSizeEW = NULL;

	RECT main;
	GetWindowRect(hWnd, &main);
	int nStatusSize[] = { main.right * 6 / 9, main.right * 6 / 9 + main.right / 12, -1 };

	switch (message)
	{
	case WM_CREATE:
	{
		GetWindowRect(hWnd, &rect);
		
		g_Drive = new Drive();
		g_Drive->getSystemDrives();

		nleftWnd_width = LEFT_WINDOW_WIDTH;

		// Ensure that the common control DLL is loaded. 
		InitCommonControls();

		//Create treeview
		long extStyle = 0;


		int x = 0;
		int y = 0;
		int nWidth = LEFT_WINDOW_WIDTH;
		int nHeight = rect.bottom - rect.top - (TOP_POS + BOTTOM_POS);

		g_hTreeView = createTreeView(extStyle, hWnd, IDC_TREEVIEW, g_hInstance, x, y, nWidth, nHeight);
		loadToTreeView(g_Drive, g_hTreeView);
		SetFocus(g_hTreeView);
		ShowWindow(g_hTreeView, SW_SHOW);
		UpdateWindow(g_hTreeView);
		
		//create listview
		x = nWidth;
		nWidth = rect.right - rect.left - LEFT_WINDOW_WIDTH - SPLITTER_BAR_WIDTH;
		extStyle = WS_EX_CLIENTEDGE;

		g_hListView = createListView(extStyle, hWnd, IDC_LISTVIEW, g_hInstance, x, y, nWidth, nHeight);
		loadToListView(g_Drive, g_hListView);
		ShowWindow(g_hListView, SW_SHOW);
		UpdateWindow(g_hListView);

		g_hStatusBar = CreateWindow(STATUSCLASSNAME, NULL,
			WS_CHILD | WS_VISIBLE | SBARS_SIZEGRIP,
			0, 0, CW_USEDEFAULT, 100, hWnd, (HMENU)IDC_STATUSBAR, hInst, NULL);

		SendMessage(g_hStatusBar, SB_SETPARTS, 3, (LPARAM)&nStatusSize);
		MoveWindow(g_hStatusBar, 0, 0, rect.right, rect.bottom, TRUE);

	}
		break;
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_NOTIFY:
	{
		int nCurSelIndex;

		//The program has started and loaded all necessary component

		NMHDR* notifyMess = (NMHDR*)lParam; //Notification Message
		LPNMTREEVIEW lpnmTree = (LPNMTREEVIEW)notifyMess; //Contains information about a tree-view notification message
		HTREEITEM currSelected;

		switch (notifyMess->code)
		{
		case TVN_ITEMEXPANDING: //This event fire when user expand or colapse item in Tree View
			//Load child item of current selected child if they were not loaded before
			currSelected = lpnmTree->itemNew.hItem;
			loadExpandedChild(currSelected, g_hTreeView);
			break;
		case TVN_SELCHANGED:
			//Retrieve currently selected item in TreeView
			currSelected = TreeView_GetSelection(g_hTreeView); //You can explicitly get by TreeView_GetNextItem with TVGN_CARET flag
			TreeView_Expand(g_hTreeView, currSelected, TVE_EXPAND);
			ListView_DeleteAllItems(g_hListView); //Clear ListView
			loadListviewItemAt(getPath(currSelected, g_hTreeView), hWnd, g_hListView, g_Drive);
			break;

		case NM_CLICK:
			nCurSelIndex = ListView_GetNextItem(GetDlgItem(hWnd, IDC_LISTVIEW), -1, LVNI_FOCUSED);
			LVITEM lv;
			lv.mask = LVIF_TEXT;
			lv.iItem = nCurSelIndex;
			lv.iSubItem = 0;
			lv.pszText = text;
			lv.cchTextMax = 256;
			ListView_GetItem(g_hListView, &lv);
			SendMessage(GetDlgItem(hWnd, IDC_STATUSBAR), SB_SETTEXT, 1, (LPARAM)text);
			lv.iSubItem = 2;
			ListView_GetItem(g_hListView, &lv);

			if (!StrCmpI(lv.pszText, _T("File folder")))
				SendMessage(GetDlgItem(hWnd, IDC_STATUSBAR), SB_SETTEXT, 1, NULL);
			else
			{
				lv.iSubItem = 3;
				ListView_GetItem(g_hListView, &lv);
				SendMessage(GetDlgItem(hWnd, IDC_STATUSBAR), SB_SETTEXT, 1, (LPARAM)text);
			}
			break;

		case NM_DBLCLK:
			//Get hwndFrom for window handle to the control sending the message
			//To check whether this event fire by Listview
			if (notifyMess->hwndFrom == g_hListView)
				loadOrExecSelected(g_hListView);
			break;
		}
	}
		break;
	case WM_LBUTTONDOWN:
	{
		int xPos;
		int yPos;

		// Varible used to get the mouse cursor x and y co-ordinates
		xPos = (int)LOWORD(lParam);
		yPos = (int)HIWORD(lParam);

		// Checks whether the mouse is over the splitter window
		xSizing = (xPos > nleftWnd_width - SPLITTER_BAR_WIDTH && xPos < nleftWnd_width + SPLITTER_BAR_WIDTH);

		// If the mouse is over the splitter then set mouse cursor 
		// image to IDC_SIZEWE which helps the user to drag the window
		if (xSizing)
		{
			// Api to capture mouse input
			SetCapture(hWnd);
			if (xSizing)
			{
				SetCursor(hcSizeEW);
			}
		}
	}
		break;
	case WM_MOUSEMOVE:
	{
		int xPos;
		int yPos;

		// Get the x and y co-ordinates of the mouse
		xPos = (int)LOWORD(lParam);
		yPos = (int)HIWORD(lParam);

		if (xPos < LEFT_MINIMUM_SPACE || xPos > RIGHT_MINIMUM_SPACE)
		{
			return 0;
		}

		// Checks if the left button is pressed during dragging the splitter
		if (wParam == MK_LBUTTON)
		{
			// If the window is d`agged using the splitter, get the
			// cursors current postion and draws a focus rectangle 
			if (xSizing)
			{
				RECT focusrect;
				HDC hdc;

				hdc = GetDC(hWnd);
				GetClientRect(hWnd, &rect);

				if (xSizing)
				{
					SetRect(&focusrect, nleftWnd_width - (WIDTH_ADJUST * 2), rect.top + TOP_POS,
						nleftWnd_width + WIDTH_ADJUST,
						rect.bottom - BOTTOM_POS);

					DrawFocusRect(hdc, &focusrect);

					// Get the size of the left window to increase
					nleftWnd_width = xPos;

					// Draws a focus rectangle
					SetRect(&focusrect, nleftWnd_width - (SPLITTER_BAR_WIDTH * 2), rect.top + 80,
						nleftWnd_width + SPLITTER_BAR_WIDTH,
						rect.bottom - BOTTOM_POS);

					DrawFocusRect(hdc, &focusrect);

				}
				ReleaseDC(hWnd, hdc);
			}
		}
		// Set the cursor image to east west direction when the mouse is over 
		// the splitter window
		if ((xPos > nleftWnd_width - SPLITTER_BAR_WIDTH && xPos < nleftWnd_width + SPLITTER_BAR_WIDTH))
		{
			SetCursor(hcSizeEW);
		}
	}
		break;

	case WM_LBUTTONUP:
		if (xSizing)
		{
			RECT focusrect;
			HDC hdc;

			// Releases the captured mouse input
			ReleaseCapture();
			// Get the main window dc to draw a focus rectangle
			hdc = GetDC(hWnd);
			GetClientRect(hWnd, &rect);
			if (xSizing)
			{
				SetRect(&focusrect, nleftWnd_width - (WIDTH_ADJUST * 2), rect.top + TOP_POS,
					nleftWnd_width + WIDTH_ADJUST,
					rect.bottom - 80);

				// Call api to vanish the dragging rectangle 
				DrawFocusRect(hdc, &focusrect);

				xSizing = FALSE;

			}
			// Release the dc once done 
			ReleaseDC(hWnd, hdc);
		}
		// Post a WM_SIZE message to redraw the windows
		PostMessage(hWnd, WM_SIZE, 0, 0);
		break;
	case WM_SIZE:
		GetClientRect(hWnd, &rect);
		MoveWindow(g_hTreeView, rect.left,
			rect.top + TOP_POS,
			rect.left + (nleftWnd_width - WIDTH_ADJUST),
			(rect.bottom - (TOP_POS + BOTTOM_POS)),
			FALSE);

		MoveWindow(g_hListView, rect.left + nleftWnd_width + WIDTH_ADJUST,
			rect.top + TOP_POS,
			rect.right - (nleftWnd_width + WIDTH_ADJUST),
			rect.bottom - (TOP_POS + BOTTOM_POS),
			FALSE);

		InvalidateRect(hWnd, &rect, TRUE);

		MoveWindow(g_hStatusBar, 0, 0, main.right, main.bottom, TRUE);
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		GetClientRect(hWnd, &rect);
		saveIniconfig(rect.right - rect.left + 16, rect.bottom - rect.top + 59);
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}


HWND createTreeView(long lExtStyle, HWND parentWnd, long ID, HINSTANCE hParentInst, int x, int y, int nWidth, int nHeight)
{
	HWND m_hTreeView = CreateWindowEx(lExtStyle, WC_TREEVIEW, _T("Tree View"),
		WS_CHILD | WS_VISIBLE | WS_BORDER | WS_VSCROLL | WS_TABSTOP | TVS_HASLINES | TVS_LINESATROOT | TVS_HASBUTTONS | TVS_SHOWSELALWAYS,
		x, y, nWidth, nHeight, parentWnd, (HMENU)ID, hParentInst, NULL);

	return m_hTreeView;
}

void loadToTreeView(Drive *drive, HWND m_hTreeView)
{
	imageTree();
	TV_INSERTSTRUCT tvInsert;

	tvInsert.item.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_PARAM;

	//Desktop
	tvInsert.hParent = NULL;
	tvInsert.hInsertAfter = TVI_ROOT;
	tvInsert.item.iImage = IDI_DESKTOP;
	tvInsert.item.iSelectedImage = IDI_DESKTOP;
	tvInsert.item.pszText = _T("Desktop");
	tvInsert.item.lParam = (LPARAM)_T("Desktop");
	HTREEITEM hDesktop = TreeView_InsertItem(m_hTreeView, &tvInsert);

	//Computer
	tvInsert.hParent = hDesktop;
	tvInsert.hInsertAfter = TVI_LAST;
	tvInsert.item.iImage = IDI_MYCOMPUTER;
	tvInsert.item.iSelectedImage = IDI_MYCOMPUTER;
	tvInsert.item.pszText = _T("My Computer");
	tvInsert.item.lParam = (LPARAM)_T("MyComputer");
	HTREEITEM hMyComputer = TreeView_InsertItem(m_hTreeView, &tvInsert);

	//Load volume
	for (int i = 0; i < g_Drive->getCount(); ++i)
	{
		tvInsert.hParent = hMyComputer;
		tvInsert.item.iImage = drive->GetIconIndex(i);
		tvInsert.item.iSelectedImage = drive->GetIconIndex(i);
		tvInsert.item.pszText = g_Drive->getDisplayName(i);//Get volume label
		tvInsert.item.lParam = (LPARAM)g_Drive->getDriveLetter(i);
		HTREEITEM hDrive = TreeView_InsertItem(m_hTreeView, &tvInsert);

		loadTreeviewItemAt(hDrive, getPath(hDrive, m_hTreeView), m_hTreeView);
	}

	//Expand and select My Computer
	TreeView_Expand(m_hTreeView, hMyComputer, TVE_EXPAND);
	TreeView_SelectItem(m_hTreeView, hMyComputer);
}

void imageTree()
{
	HIMAGELIST *himage = new HIMAGELIST;
	*himage = ImageList_Create(16, 16, ILC_COLOR32 | ILC_MASK, 8, 0);
	HICON hicon0 = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON0));
	ImageList_AddIcon(*himage, hicon0);


	HICON hicon1 = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON1));
	ImageList_AddIcon(*himage, hicon1);


	HICON hicon2 = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON2));
	ImageList_AddIcon(*himage, hicon2);


	HICON hicon3 = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON3));
	ImageList_AddIcon(*himage, hicon3);


	HICON hicon4 = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON4));
	ImageList_AddIcon(*himage, hicon4);


	HICON hicon5 = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON5));
	ImageList_AddIcon(*himage, hicon5);


	HICON hicon6 = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON6));
	ImageList_AddIcon(*himage, hicon6);


	HICON hicon7 = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON7));
	ImageList_AddIcon(*himage, hicon7);

	TreeView_SetImageList(g_hTreeView, *himage, TVSIL_NORMAL);
}

void loadTreeviewItemAt(HTREEITEM &hParent, LPCWSTR path, HWND m_hTreeView)
{
	TCHAR buffer[MAX_PATH_LEN];
	StrCpy(buffer, path);

	StrCat(buffer, _T("\\*"));

	TV_INSERTSTRUCT tvInsert;
	tvInsert.hParent = hParent;
	tvInsert.hInsertAfter = TVI_LAST;
	tvInsert.item.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_PARAM;
	tvInsert.item.iImage = IDI_FOLDER;
	tvInsert.item.iSelectedImage = IDI_FOLDER;

	WIN32_FIND_DATA fd;
	HANDLE hFile = FindFirstFileW(buffer, &fd);

	if (hFile == INVALID_HANDLE_VALUE)
		return;

	TCHAR * folderPath;
	do
	{
		if ((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			&& ((fd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) != FILE_ATTRIBUTE_HIDDEN)
			&& ((fd.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) != FILE_ATTRIBUTE_SYSTEM)
			&& (StrCmp(fd.cFileName, _T(".")) != 0) && (StrCmp(fd.cFileName, _T("..")) != 0))
		{
			tvInsert.item.pszText = fd.cFileName;
			folderPath = new TCHAR[wcslen(path) + wcslen(fd.cFileName) + 2];

			StrCpy(folderPath, path);
			if (wcslen(path) != 3)
				StrCat(folderPath, _T("\\"));
			StrCat(folderPath, fd.cFileName);

			tvInsert.item.lParam = (LPARAM)folderPath;
			HTREEITEM hItem = TreeView_InsertItem(m_hTreeView, &tvInsert);

		}
	} while (FindNextFileW(hFile, &fd));
}

void loadExpandedChild(HTREEITEM hCurrSelected, HWND m_hTreeView)
{
	HTREEITEM myDesktop = TreeView_GetRoot(m_hTreeView); //Return the top most or very first item of the TreeView
	HTREEITEM myComputer = TreeView_GetChild(m_hTreeView, myDesktop); //Return the first child item of Desktop (MyComputer)
	if (hCurrSelected == myComputer) //If currently select My Computer -> dont load
		return;
	HTREEITEM hCurrSelectedChild = TreeView_GetChild(m_hTreeView, hCurrSelected); //Get the first child of treeview item

	if (hCurrSelectedChild != NULL)
	{
		do
		{
			//Get child of this Current selected child, if result is NULL -> It never be loaded
			if (TreeView_GetChild(m_hTreeView, hCurrSelectedChild) == NULL)
			{
				//Load all child of Current Selected Child	
				loadTreeviewItemAt(hCurrSelectedChild, getPath(hCurrSelectedChild, m_hTreeView), m_hTreeView);
			}
		} while (hCurrSelectedChild = TreeView_GetNextSibling(m_hTreeView, hCurrSelectedChild));

	}
	else
	{
		loadTreeviewItemAt(hCurrSelected, getPath(hCurrSelected, m_hTreeView), m_hTreeView);
	}
}

LPCWSTR getPath(HTREEITEM hItem, HWND m_hTreeView)
{
	TVITEMEX tv; //Specifies or receives attributes of a tree-view item.
	tv.mask = TVIF_PARAM;
	tv.hItem = hItem;
	TreeView_GetItem(m_hTreeView, &tv);
	return (LPCWSTR)tv.lParam;
}


HWND createListView(long lExtStyle, HWND parentWnd, long ID, HINSTANCE hParentInst, int x, int y, int nWidth, int nHeight)
{
	//Create
	HWND m_hListView = CreateWindowEx(lExtStyle, WC_LISTVIEW, _T("List View"),
		WS_CHILD | WS_VISIBLE | LVS_REPORT | LVS_ICON | LVS_EDITLABELS | LVS_SHOWSELALWAYS,
		x, y, nWidth, nHeight, parentWnd, (HMENU)ID, hParentInst, NULL);


	//Init 5 columns
	LVCOLUMN lvCol;

	//Let the LVCOLUMN know that we will set the format, header text and width of it
	lvCol.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvCol.fmt = LVCFMT_LEFT;

	lvCol.cx = 150;
	lvCol.pszText = _T("Name");
	ListView_InsertColumn(m_hListView, 0, &lvCol);

	lvCol.fmt = LVCFMT_LEFT;
	lvCol.pszText = _T("Type");
	lvCol.cx = 125;
	ListView_InsertColumn(m_hListView, 1, &lvCol);

	lvCol.fmt = LVCFMT_LEFT;
	lvCol.cx = 125;
	lvCol.pszText = _T("Total Size");
	ListView_InsertColumn(m_hListView, 2, &lvCol);


	lvCol.fmt = LVCFMT_LEFT;
	lvCol.pszText = _T("Free Space");
	lvCol.cx = 125;
	ListView_InsertColumn(m_hListView, 3, &lvCol);

	return m_hListView;
}

void loadToListView(Drive *drive, HWND m_hListView)
{
	//Init column of Listview
	initListviewColumn(m_hListView, LVCOL_DRIVE_TYPE);
	imageListView();
	LV_ITEM lv;

	for (int i = 0; i < drive->getCount(); ++i)
	{
		//Let ListView know that we'r going to change item text, image and param
		lv.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
		lv.iItem = i;
		lv.iImage = drive->GetIconIndex(i);
		lv.iSubItem = 0;
		lv.pszText = drive->getDisplayName(i);
		lv.lParam = (LPARAM)drive->getDriveLetter(i);
		ListView_InsertItem(m_hListView, &lv);

		//
		lv.mask = LVIF_TEXT;
		lv.iSubItem = 1;
		lv.pszText = drive->getDriveType(i);
		ListView_SetItem(m_hListView, &lv);

		lv.iSubItem = 2;
		if (wcscmp(drive->getDriveType(i), CD_ROM) != 0)
			lv.pszText = drive->getTotalSize(i);
		else
			lv.pszText = NULL;
		ListView_SetItem(m_hListView, &lv);

		lv.iSubItem = 3;
		if (wcscmp(drive->getDriveType(i), CD_ROM) != 0)
			lv.pszText = drive->getFreeSpace(i);
		else
			lv.pszText = NULL;

		ListView_SetItem(m_hListView, &lv);
	}
}

void loadListviewItemAt(LPCWSTR path, HWND m_hParent, HWND m_hListView, Drive *drive)
{
	//If path is NULL, quit
	if (path == NULL)
		return;

	LV_ITEM lv;

	if (_tcscmp(path, _T("Desktop")) == 0)
	{
		//Load Desktop to Listview (My Computer)
		initListviewColumn(m_hListView, LVCOL_FOLDER_TYPE);

		lv.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
		lv.iItem = 0;
		lv.iSubItem = 0;
		lv.pszText = _T("My Computer");
		lv.iImage = IDI_MYCOMPUTER;
		lv.lParam = (LPARAM)_T("MyComputer");
		ListView_InsertItem(m_hListView, &lv);
	}
	else if (_tcscmp(path, _T("MyComputer")) == 0)
	{
		//Load My Computer to Listview (Drives, Volume,..)
		initListviewColumn(m_hListView, LVCOL_DRIVE_TYPE);

		for (int i = 0; i < drive->getCount(); ++i)
		{
			lv.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
			lv.iItem = i;
			lv.iImage = DEFAULT_ICON_INDEX;
			lv.iSubItem = 0;
			lv.pszText = drive->getDisplayName(i);
			lv.lParam = (LPARAM)drive->getDriveLetter(i);
			ListView_InsertItem(m_hListView, &lv);

			lv.mask = LVIF_TEXT;
			lv.iSubItem = 1;
			lv.pszText = drive->getDriveType(i);

			ListView_SetItem(m_hListView, &lv);
			lv.iSubItem = 2;
			if (wcscmp(drive->getDriveType(i), CD_ROM) != 0)
				lv.pszText = drive->getTotalSize(i);
			else
				lv.pszText = NULL;
			ListView_SetItem(m_hListView, &lv);

			lv.iSubItem = 3;
			if (wcscmp(drive->getDriveType(i), CD_ROM) != 0)
				lv.pszText = drive->getFreeSpace(i);
			else
				lv.pszText = NULL;
			ListView_SetItem(m_hListView, &lv);
		}
	}
	else
		loadDirItemToListview(g_hWnd, m_hListView, path);
}

void initListviewColumn(HWND m_hListView, int type)
{
	LVCOLUMN lvCol;
	if (type == LVCOL_DRIVE_TYPE)
	{
		lvCol.mask = LVCF_TEXT | LVCF_FMT;

		lvCol.fmt = LVCFMT_LEFT | LVCF_WIDTH;
		lvCol.cx = 100;
		lvCol.pszText = _T("Type");
		ListView_SetColumn(m_hListView, 1, &lvCol);

		lvCol.fmt = LVCFMT_RIGHT | LVCF_WIDTH;
		lvCol.cx = 80;
		lvCol.pszText = _T("Total Size");
		ListView_SetColumn(m_hListView, 2, &lvCol);

		lvCol.cx = 80;
		lvCol.pszText = _T("Free Space");
		ListView_SetColumn(m_hListView, 3, &lvCol);
	}
	else if (type == LVCOL_FOLDER_TYPE)
	{
		lvCol.mask = LVCF_WIDTH;
		lvCol.cx = 180;
		ListView_SetColumn(m_hListView, 0, &lvCol);

		lvCol.mask = LVCF_TEXT | LVCF_FMT;
		lvCol.fmt = LVCFMT_RIGHT;
		lvCol.pszText = _T("Date Modified");
		ListView_SetColumn(m_hListView, 1, &lvCol);


		lvCol.mask = LVCF_TEXT | LVCF_WIDTH | LVCF_FMT;
		lvCol.fmt = LVCFMT_LEFT;
		lvCol.cx = 130;
		lvCol.pszText = _T("Type");
		ListView_SetColumn(m_hListView, 2, &lvCol);

		lvCol.pszText = _T("Size");
		ListView_SetColumn(m_hListView, 3, &lvCol);
	}
	else
	{
		//Reserve for initialize or create LV
	}
}

void loadDirItemToListview(HWND m_hParent, HWND m_hListView, LPCWSTR path)
{
	initListviewColumn(m_hListView, LVCOL_FOLDER_TYPE);
	TCHAR buffer[10240];

	//Copy path to buffer
	StrCpy(buffer, path);

	if (wcslen(path) == 3)
		StrCat(buffer, _T("*"));
	else
		StrCat(buffer, _T("\\*"));

	//Variables
	WIN32_FIND_DATA fd; //Contains information about the file that is found by the FindFirstFile or FindNextFile function
	HANDLE hFind = INVALID_HANDLE_VALUE;
	LV_ITEM lv;
	TCHAR* temporaryPath;
	int itemIndex = 0;


	//Find file and folder in this directory
	//Get search handle to search folder 
	hFind = FindFirstFileW(buffer, &fd);

	//If the function fails or fails to locate files from the search string
	if (hFind == INVALID_HANDLE_VALUE)
		return;

	//Iterator
	do
	{
		//Get only folder
		if ((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) &&
			((fd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) != FILE_ATTRIBUTE_HIDDEN) &&
			(_tcscmp(fd.cFileName, _T(".")) != 0) && (_tcscmp(fd.cFileName, _T("..")) != 0)) //Ignore . (curr dir) and .. (parent dir)
		{
			//Get path of this folder
			temporaryPath = new TCHAR[wcslen(path) + wcslen(fd.cFileName) + 2];
			StrCpy(temporaryPath, path);

			if (wcslen(path) != 3)
				StrCat(temporaryPath, _T("\\"));

			StrCat(temporaryPath, fd.cFileName);


			//Add name and path to first column
			//Name: fd.cFileName
			//Path: (LPARAM)temporaryPath
			lv.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
			lv.iItem = itemIndex;
			lv.iSubItem = 0;
			lv.pszText = fd.cFileName;
			lv.iImage = DEFAULT_ICON_INDEX;
			lv.lParam = (LPARAM)temporaryPath;
			ListView_InsertItem(m_hListView, &lv);

			//Second column is Date Modified
			//fd.ftLastWriteTime is the number of 100-nanosecond intervals since January 1, 1601 (UTC)
			ListView_SetItemText(m_hListView, itemIndex, 1, convertTimeStampToString(fd.ftLastWriteTime)); //Changes the text of a list - view item or subitem

			//Add "File folder" value to Third column
			ListView_SetItemText(m_hListView, itemIndex, 2, _T("File folder"));

			//Last column is Size
			//Let check Explorer whether it show size of file or folder

			//Increase the index
			itemIndex++;
		}

		//Continues a file search from a previous call to the FindFirstFileW function
		//Return non-zero if successfully found, otherwise return zero
	} while (FindNextFileW(hFind, &fd));


	//Get all file in this directory
	//Get search handle to search file 
	hFind = FindFirstFileW(buffer, &fd);

	if (hFind == INVALID_HANDLE_VALUE)
		return;

	//Iterator
	do
	{
		//Ignore all Directory and Folder
		if (((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != FILE_ATTRIBUTE_DIRECTORY) &&
			((fd.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) != FILE_ATTRIBUTE_SYSTEM) &&
			((fd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) != FILE_ATTRIBUTE_HIDDEN))
		{
			//Get file path
			temporaryPath = new TCHAR[wcslen(path) + wcslen(fd.cFileName) + 2];
			StrCpy(temporaryPath, path);

			if (wcslen(path) != 3)
				StrCat(temporaryPath, _T("\\"));

			StrCat(temporaryPath, fd.cFileName);

			//Add name and path to first column
			lv.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
			lv.iItem = itemIndex;
			lv.iSubItem = 0;
			lv.pszText = fd.cFileName;
			lv.iImage = 1;
			lv.lParam = (LPARAM)temporaryPath;

			ListView_InsertItem(m_hListView, &lv);

			//Second column is Date Modified
			ListView_SetItemText(m_hListView, itemIndex, 1, convertTimeStampToString(fd.ftLastWriteTime));

			//Third column is Type
			//ListView_SetItemText(m_hListView, itemIndex, 2, _GetType(fd));

			//Last column is Size
			DWORD fileSizeLow = fd.nFileSizeLow; //The low-order DWORD value of the file size, in bytes
			ListView_SetItemText(m_hListView, itemIndex, 3, DriveSize::Convert(fileSizeLow));

			itemIndex++;
		}
	} while (FindNextFileW(hFind, &fd));

}

void loadOrExecSelected(HWND m_hListView)
{
	LPCWSTR filePath = getPath(m_hListView, ListView_GetSelectionMark(m_hListView));

	WIN32_FIND_DATA fd;

	//Retrieves attributes for a specified file or directory.
	if (GetFileAttributesEx(filePath, GetFileExInfoStandard, &fd) != 0)
	{
		//Check whether it's folder or directory
		if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			//Delete and reload item in Listview
			ListView_DeleteAllItems(m_hListView);
			loadDirItemToListview(g_hWnd, m_hListView, filePath);
		}
		else
		{
			//If it's file -> run it
			//ShellExecute is a function to Open specified file or folder with lpOperation _T("open")
			//specify "SW_SHOWNORMAL" flag for displaying the window for the first time
			ShellExecute(NULL, _T("open"), filePath, NULL, NULL, SW_SHOWNORMAL);
		}
	}
}

LPCWSTR getPath(HWND m_hListView, int iItem)
{
	LVITEM lv;
	lv.mask = LVIF_PARAM;
	lv.iItem = iItem;
	lv.iSubItem = 0;
	ListView_GetItem(m_hListView, &lv);
	return (LPCWSTR)lv.lParam;
}

LPWSTR convertTimeStampToString(const FILETIME &ftLastWrite)
{
	TCHAR *buffer = new TCHAR[50];
	SYSTEMTIME st;

	char szLocalDate[255], szLocalTime[255];

	FileTimeToSystemTime(&ftLastWrite, &st);
	GetDateFormat(LOCALE_USER_DEFAULT, DATE_AUTOLAYOUT, &st, NULL,
		(LPWSTR)szLocalDate, 255);
	GetTimeFormat(LOCALE_USER_DEFAULT, 0, &st, NULL, (LPWSTR)szLocalTime, 255);

	//Concat to string
	wsprintf(buffer, L"%s %s", szLocalDate, szLocalTime);

	return buffer;
}

void imageListView()
{
	HIMAGELIST *himage = new HIMAGELIST;
	*himage = ImageList_Create(16, 16, ILC_COLOR32 | ILC_MASK, 8, 1);
	HICON hicon0 = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON0));
	ImageList_AddIcon(*himage, hicon0);
	ListView_SetImageList(g_hListView, *himage, LVSIL_SMALL);

	HICON hicon1 = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON4));
	ImageList_AddIcon(*himage, hicon1);
	ListView_SetImageList(g_hListView, *himage, LVSIL_SMALL);

	HICON hicon2 = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON2));
	ImageList_AddIcon(*himage, hicon2);
	ListView_SetImageList(g_hListView, *himage, LVSIL_SMALL);

	HICON hicon3 = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON3));
	ImageList_AddIcon(*himage, hicon3);
	ListView_SetImageList(g_hListView, *himage, LVSIL_SMALL);

	HICON hicon4 = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON1));
	ImageList_AddIcon(*himage, hicon4);
	ListView_SetImageList(g_hListView, *himage, LVSIL_SMALL);

	HICON hicon5 = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON5));
	ImageList_AddIcon(*himage, hicon5);
	ListView_SetImageList(g_hListView, *himage, LVSIL_SMALL);

	HICON hicon6 = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON6));
	ImageList_AddIcon(*himage, hicon6);
	ListView_SetImageList(g_hListView, *himage, LVSIL_SMALL);

	HICON hicon7 = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON7));
	ImageList_AddIcon(*himage, hicon7);
	ListView_SetImageList(g_hListView, *himage, LVSIL_SMALL);
}

void saveIniconfig(int nwidth, int nheight)
{
	WCHAR buffer[BUFFERSIZE];
	swprintf_s(buffer, L"%d", nwidth);
	WritePrivateProfileString(L"app", L"width", buffer, configPath);
	swprintf_s(buffer, L"%d", nheight);
	WritePrivateProfileString(L"app", L"height", buffer, configPath);

}

void loadIniconfig(int &nwidth, int &nheight)
{
	WCHAR buffer[BUFFERSIZE];
	GetPrivateProfileString(L"app", L"width", L"Default value", buffer, BUFFERSIZE, configPath);
	nwidth = _wtoi(buffer);
	GetPrivateProfileString(L"app", L"height", L"Default value", buffer, BUFFERSIZE, configPath);
	nheight = _wtoi(buffer);

}