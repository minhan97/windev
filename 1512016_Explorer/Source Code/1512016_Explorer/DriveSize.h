#pragma once
class DriveSize
{
	__int64 totalSize;
	__int64 freeSpace;
public:
	DriveSize();
	~DriveSize();
	DriveSize(__int64, __int64);
	static LPWSTR Convert(__int64 nSize);

	LPWSTR getTotalSizeStr();
	LPWSTR getFreeSpaceStr();
};

