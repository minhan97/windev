+ Info:
Lê Minh Ân
1512016

+ Các chức năng đã thực hiện:
1. Tạo ra giao diện có một edit control để nhập liệu.
2. Lưu lại dưới dạng file text. Dùng hộp thoại SaveFileDialog để lưu file (nhưng chỉ lưu file tạo mới, không lưu được file đang mở)
3. Mở một tập tin, đọc nội dung và hiển thị để chỉnh sửa tiếp. Sử dụng OpenFileDialog.
4. Hỗ trợ các thao tác Cut - Copy - Paste - Select All text - New - Change font.
+Luồng sự kiện chính : cung cấp giao diện làm việc, tạo và lưu tập tin text như notepad
+Luồng sự kiện phụ: cung cấp thêm các chức năng thay đổi font chữ, thay đổi kích cỡ edit text chính khi main window thay đổi kích thước, tạo mới.

+Link bitbucket:
SSH: git clone git@bitbucket.org:minhan97/windev.git
HTPPS: git clone https://minhan97@bitbucket.org/minhan97/windev.git

